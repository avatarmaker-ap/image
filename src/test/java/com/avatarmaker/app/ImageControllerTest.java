package com.avatarmaker.app;

import com.avatarmaker.app.controller.ImageController;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

/**
 * Test for ImageController
 * @author Dave Nathanael
 */
@RunWith(SpringRunner.class)
@WebMvcTest(ImageController.class)
public class ImageControllerTest {
    /**
     * MockMvc to perform HTTP requests
     */
    @Autowired
    private MockMvc mvc;

    /**
     * Jackson object serializer/deserializer helper
     */
    @Autowired
    private ObjectMapper objectMapper;


    /**
     * Test to download an image, with a list of URL(s)
     * as the request body
     * @throws Exception
     */
    @Test
    public void tryDownloadImage() throws Exception {
        List<String> urls = new ArrayList<String>();
        urls.add("https://source.unsplash.com/random/800x600");

        mvc.perform(
            post("/download")
            .content(objectMapper.writeValueAsString(urls))
            .contentType(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.IMAGE_JPEG));
    }
}

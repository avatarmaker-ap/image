package com.avatarmaker.app.utils;

import static org.junit.Assert.assertEquals;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ImageCombinerTest {
    @Test
    public void getImageTest() {
        String url = "https://source.unsplash.com/random/800x600";
        BufferedImage img = ImageCombiner.getImage(url);
        assertEquals(800, img.getWidth());
        assertEquals(600, img.getHeight());
    }

    @Test
    public void getImageWithWrongUrlTest() {
        String url = "wrongUrl";
        BufferedImage img = ImageCombiner.getImage(url);
        assertEquals(1, img.getWidth());
        assertEquals(1, img.getHeight());
    }

    @Test
    public void combinedHeightTestWithNoUrl() {
        List<String> urls = new ArrayList<String>();

        BufferedImage img = ImageCombiner.getCombinedImage(urls);
        assertEquals(1, img.getWidth());
        assertEquals(1, img.getHeight());
    }

    @Test
    public void combinedHeightTestWithOneUrl() {
        List<String> urls = new ArrayList<String>();
        urls.add("https://source.unsplash.com/random/800x600");

        BufferedImage img = ImageCombiner.getCombinedImage(urls);
        assertEquals(800, img.getWidth());
        assertEquals(600, img.getHeight());
    }

    @Test
    public void combinedHeightTestWithMoreThanOneUrls() {
        List<String> urls = new ArrayList<String>();
        urls.add("https://source.unsplash.com/random/800x600");
        urls.add("https://source.unsplash.com/random/800x600");
        urls.add("https://source.unsplash.com/random/800x600");

        BufferedImage img = ImageCombiner.getCombinedImage(urls);
        assertEquals(800, img.getWidth());
        assertEquals(600, img.getHeight()); //assert that the height is the same (because images is overlayed)
    }

}

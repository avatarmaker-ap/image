package com.avatarmaker.app.utils;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
/** 
 * ImageCombiner is a utility class used to get image(s) from its URL and
 * combine several images into one
 * 
 * All BufferedImage used BufferedImage.TYPE_INT_ARGB to accomodate PNG format
 * 
 * @author Dave Nathanael
 */
public class ImageCombiner {
    /**
     * Get the BufferedImage representation of an image from a given URL
     * @param imageUrl URL of the image
     * @return the BufferedImage object
     */
    public static BufferedImage getImage(String imageUrl){
        try {
            URL url = new URL(imageUrl);
            return ImageIO.read(url);
        } catch (MalformedURLException me) {
            return new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
        } catch (IOException ie) {
            return new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
        }
    }

    /**
     * Get a list of BufferedImage representation of images from
     * a given url list
     * @param list of image URL(s)
     * @return list of BufferedImage object(s)
     */
    public static List<BufferedImage> getImages(List<String> urls) {
        List<BufferedImage> images = new ArrayList<>();
        for (String url : urls) {
            BufferedImage img = getImage(url);
            images.add(img);
        }

        return images;
    }

    /**
     * Combine image(s) from given URL(s) list and return it
     * @param urls list of image URL(s)
     * @return the combined result BufferedImage object
     */
    public static BufferedImage getCombinedImage(List<String> urls) {
        // Edge cases if given url list contains 0 or 1 URL only
        if (urls.size() == 0)
            return new BufferedImage(1, 1,
                BufferedImage.TYPE_INT_ARGB
            );
        if (urls.size() == 1)
            return getImage(urls.get(0));
        
        List<BufferedImage> images = getImages(urls);
        BufferedImage combined = new BufferedImage(
            images.get(0).getWidth(),
            images.get(0).getHeight(),
            BufferedImage.TYPE_INT_ARGB
        );
        Graphics graphics = combined.getGraphics();

        // Overlap every next image available above previous images
        for (int i = 0; i < images.size(); i++) {
            graphics.drawImage(images.get(i), 0, 0, null);
        }
        return combined;
    }
}
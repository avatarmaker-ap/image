package com.avatarmaker.app.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.validation.Valid;

import com.avatarmaker.app.utils.ImageCombiner;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for this service (image service)
 * only contains an endpoint to download combined image(s)
 * 
 * Have localhost and netlify domain as the allowed origins
 * 
 * @author Dave Nathanael
 */
@CrossOrigin(origins = {
    "http://localhost:3000",
    "https://avatarmaker.netlify.com",
    "https://avatarmaker.netlify.app"
})
@RestController
public class ImageController {

    /**
     * Combines images from given URL(s) and return
     * the combined image as attachment
     * @param imageUrls list of image URL(s) given as the request body
     * @return response with the image attached (see header)
     */
    @PostMapping(
        value = "/download",
        produces = MediaType.IMAGE_JPEG_VALUE
    )
    public ResponseEntity<byte[]> getImage(
        @RequestBody @Valid List<String> imageUrls
    ){
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            ImageIO.write(
                ImageCombiner.getCombinedImage(imageUrls),
                "PNG",
                out
            );
        } catch (IOException ie) {
            return ResponseEntity.ok().body(new byte[0]);
        }

        return ResponseEntity
            .ok()
            .contentType(MediaType.IMAGE_JPEG)
            .header("Content-Disposition", "attachment; filename=image.png")
            .body(out.toByteArray());
    }
}